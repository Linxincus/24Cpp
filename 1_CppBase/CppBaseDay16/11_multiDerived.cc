#include <iostream>

using std::cout;
using std::endl;

class A
{
public:
    A(long ix = 0)
    : _ix(ix)
    {
        cout <<"A(long = 0)" << endl;
    }

    void setX(long ix)
    {
        _ix = ix;
    }

    void print() const
    {
        cout << "A::_ix = " << _ix << endl;
    }

    ~A()
    {
        cout <<"~A()" << endl;
    }
private:
    long _ix;
};

/* int A::_ix = 0; */

class B
:virtual public A
{
public:
    B()
    {
        cout <<"B()" << endl;
    }

    ~B()
    {
        cout <<"~B()" << endl;
    }
};

class C
: virtual public A
{
public:
    C()
    {
        cout << "C()" << endl;
    }

    ~C()
    {
        cout <<"~C()" << endl;
    }
};

class D
: public B
, public C
{
public:
    D()
    {
        cout <<"D()" << endl;
    }


    ~D()
    {
        cout <<"~D()" << endl;
    }
};

void test()
{
    cout <<"sizeof(A) = " << sizeof(A) << endl;//8
    cout <<"sizeof(B) = " << sizeof(B) << endl;//8
    cout <<"sizeof(C) = " << sizeof(C) << endl;//8
    cout <<"sizeof(D) = " << sizeof(D) << endl;//16
    cout << endl;
    //多继承的时候，数据成员的二义性
    //解决方案：可以让B与C虚拟继承A,在派生了D的对象中就
    //只会有一份_ix
    D d;
    /* d.print();//error */
    d.B::setX(10);
    d.B::print();

    cout << endl;
    d.C::setX(20);
    d.C::print();

    //就只想让D中只有一个_ix怎么办呢?
    d.setX(100);
    d.print();

    A a(10);
    B b;
    C c;
    printf("B = %p\n", &b);
    printf("B = %lu\n", *(long *)&b);
    printf("C = %p\n", &c);
    printf("C = %lu\n", *(long *)&c);
    printf("D = %lu\n", *(long *)&d);
    printf("D = %lu\n", *(long *)((long *)&d + 1));
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

