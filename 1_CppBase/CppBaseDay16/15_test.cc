#include <iostream>

using std::cout;
using std::endl;

class Example
{
public:
    Example()
    {
        cout <<"Example()" << endl;
    }

    ~Example()
    {
        cout <<"~Example()" << endl;
    }
private:
    //1、将拷贝构造函数设置为私有的
    Example(const Example &rhs)
    {
        cout <<"Example(const Example &)" << endl;
    }

    Example &operator=(const Example &rhs)
    {
        cout <<"Example &operator=(const Example &)" << endl;

        return *this;
    }


};

void test()
{
    Example ex1;
    Example ex2 = ex1;//拷贝构造函数,error

    cout << endl;
    Example ex3;
    ex3 = ex1;//赋值运算符函数,error
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

