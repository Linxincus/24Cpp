#include <iostream>

using std::cout;
using std::endl;

//常量位于内存中的那个位置？
//并不是所有的常量都位于同一个区域，还要看这个常量是在哪里
//定义的
//
int gValue;//全局变量，全局区,使用默认值进行初始化
char *p1;//全局变量，全局区
const int gNumber = 10;//文字常量区

int main(int argc, char *argv[])
{
    int a;//局部变量，位于栈区,默认是随机值
    char *p2;//局部变量，位于栈区
    const int number = 10;//栈区
    
    //pInt本身是位于栈区的，但是指向的变量是位于堆区的
    int *pInt = new int(10);

    static int cnt = 10;//静态变量，静态区
    //pstr本身位于栈区，但是指向的区域是文字常量区
    const char *pstr = "hello,world";

    printf("\n打印变量的地址\n");
    printf("&a = %p\n", &a);
    printf("&gValue = %p\n", &gValue);
    printf("&p2 = %p\n", &p2);
    printf("p2 = %p\n", p2);
    printf("&p1 = %p\n", &p1);
    printf("p1 = %p\n", p1);
    printf("&pInt = %p\n", &pInt);
    printf("pInt = %p\n", pInt);
    printf("&cnt = %p\n", &cnt);
    printf("&pstr = %p\n", &pstr);
    printf("pstr = %p\n", pstr);
    printf("&\"hello,world\" = %p\n", &"hello,world");
    printf("&main = %p\n", &main);
    printf("main = %p\n", main);
    printf("&gNumber = %p\n", &gNumber);
    printf("&number = %p\n", &number);

    printf("\n打印变量的值\n");
    printf("a = %d\n", a);
    printf("gValue = %d\n", gValue);

    delete pInt;
    pInt = nullptr;

    return 0;
}






