#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;
using std::vector;

void test()
{
    //对于文件输入流而言，当文件不存在的时候，就打开失败
    //当文件存在的时候，才能进行正常打开文件
    ifstream ifs("wd.txt");
    if(!ifs.good())
    {
        cerr << "ifstream is not good" << endl;
        return;
    }

    vector<string> vec;
    string line;
    //对于文件输入流而言，默认是以空格为分隔符
    while(getline(ifs, line))
    {
        vec.push_back(line);
    }

    for(size_t idx = 0; idx != vec.size(); ++idx)
    {
        cout << vec[idx] << endl;
    }
    
    cout << "vec[9] = " << vec[9] << endl;
    cout << "vec[36] = " << vec[36] << endl;
    
    ifs.close();
}
int main(int argc, char *argv[])
{
    test();
    return 0;
}

