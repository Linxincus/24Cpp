#include <iostream>
#include <log4cpp/BasicLayout.hh>
#include <log4cpp/SimpleLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/Category.hh>
#include<log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

void test()
{
    //日志的格式
    SimpleLayout *psl = new SimpleLayout();
    
    //日志的目的地
    FileAppender *pfl =  new FileAppender("1", "wd.txt");
    pfl->setLayout(psl);

    //日志记录器
    Category &root = Category::getRoot();
    root.addAppender(pfl);

    //日志的过滤器
    root.setPriority(Priority::ERROR);

    root.emerg("This is an emerg message");
    root.fatal("This is an fatal message");
    root.alert("This is an alert essage");
    root.crit("This is an crit message");
    root.error("This is an error message");
    root.notice("This is an notice message");
    root.debug("This is an debug message");
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

