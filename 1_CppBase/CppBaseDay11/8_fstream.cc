#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::fstream;
using std::string;

void test()
{
    //对于文件输入输出流而言，当文件不存在的时候，就打开失败
    //当文件存在的时候，才能进行打开成功
    string fileName("wuhan.txt");
    fstream fs(fileName);
    if(!fs.good())
    {
        cerr << "fstream is not good" << endl;
        return;
    }

    string line;
    while(getline(fs, line))
    {
        cout << line << endl;
    }

    fs.close();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

