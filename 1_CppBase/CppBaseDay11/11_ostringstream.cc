#include <iostream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;
using std::ostringstream;
using std::string;

string intToString(int value)
{
    ostringstream oss;
    oss << value;

    return oss.str();
}

void test()
{
    //想将int类型的数据通过ostringstream进行输出
    int number = 10;
    string tmp = intToString(number);
    cout << "tmp = " << tmp << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

