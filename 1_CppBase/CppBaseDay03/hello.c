#include <stdio.h>

//宏定义:在预处理阶段做字符串的替换
#define  NUM 3 + 4

//argc参数的个数
//argv将命令行参数记录在下来
int main(int argc, char *argv[])
{
    printf("num * num = %d\n", NUM  * NUM);
    return 0;
}

