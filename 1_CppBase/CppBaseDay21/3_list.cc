#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

void test()
{
    //初始化
    /* list<int> number;//1、无参对象 */
    /* list<int> number(10, 3);//2、count个value */
    //3、迭代器范围
    /* int arr[10] = {1, 3, 5, 7, 9, 8, 6, 4, 2, 10}; */
    /* list<int> number(arr, arr + 10);//[,),左闭右开的区间 */
    //4、拷贝与移动
    //5、大括号形式
    list<int> number =  {1, 3, 4, 2, 6, 8, 7, 5, 9, 10};

    //遍历
    //下标进行遍历
#if 0
    for(size_t idx = 0; idx != number.size(); ++idx)
    {
        cout << number[idx] << "  ";
    }
    cout << endl;
#endif

    //未初始化的迭代器
    list<int>::iterator it;
    for(it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;
    
    //初始化的迭代器
    list<int>::iterator it2 = number.begin();
    for(; it2 != number.end(); ++it2)
    {
        cout << *it2 << "  ";
    }
    cout << endl;

    //for与auto
    for(auto &elem : number)
    {
        cout << elem << "  ";
    }
    cout << endl;

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

