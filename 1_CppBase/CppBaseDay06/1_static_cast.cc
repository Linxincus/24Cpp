#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

//y + 目标行号 + G
//d + 目标行号 + G
void test0()
{
    int iNumber = 10;
    float fNumber = 12.34;
    iNumber = (int)fNumber;
    iNumber = int(fNumber);
}

void test()
{
    int iNumber = 10;
    float fNumber = 12.34;
    /* fNumber = (float)(iNumber);//C的写法 */
    //用于将一种数据类型转换成另一种数据类型
    fNumber = static_cast<float>(iNumber);//C++的写法

    //也可以完成指针之间的转换，
    //例如可以将void*指针转换成其他类型的指针
    void *pret = malloc(sizeof(int));
    int *pInt = static_cast<int *>(pret);

    //不能完成任意两个指针类型间的转换,error
    int iNumber2 = 1;
    int *pInt2 = &iNumber2;
    /* float *pFloat = static_cast<float *>(pInt2);//error */
}
int main(int argc, char *argv[])
{
    test();
    return 0;
}

