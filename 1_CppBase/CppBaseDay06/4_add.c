#include <stdio.h>

//C语言是不支持函数重载
int add(int x, int y)
{
    return x + y;
}

float add(float x, float y)
{
    return x + y;
}

int add(int x, float y)
{
    return x + y;
}

int add(float x, int y)
{
    return x + y;
}

int add(int x, int y, int z)
{
    return x + y + z;
}

void test()
{
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

