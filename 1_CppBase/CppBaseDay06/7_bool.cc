#include <iostream>

using std::cout;
using std::endl;

void test()
{
    //只要不是0，那就全部都是1
    bool flag = true;//1
    bool flag2 = false;//0
    bool flag3 = 100;//1
    bool a = -100;//1
    bool b = 0;//0

    cout << "sizeof(flag) = " << sizeof(flag) << endl;
    cout << "sizeof(flag3) = " << sizeof(flag3) << endl;
    cout << "sizeof(b) = " << sizeof(b) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

