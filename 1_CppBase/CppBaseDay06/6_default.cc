#include <iostream>

using std::cout;
using std::endl;

//给函数的参数设置默认值
//规则：默认参数需要从右向左进行连续的赋初值
//特点：以后可以少写代码
//int add()
//int add(int)
//int add(int, int)
//int add(int, int, int)
int add(int x = 0, int y = 10, int z = 0)
{
    return x + y + z;
}

int main(int argc, char *argv[])
{
    int a = 3, b = 4, c = 5;
    cout << "add(a, b, c) = " << add(a, b, c) << endl;
    cout << "add(a, b) = " << add(a, b) << endl;
    cout << "add(a) = " << add(a) << endl;
    cout << "add() = " << add() << endl;
    return 0;
}

