#include <stdlib.h>
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//C语言不支持函数重载，但是C++支持函数重载
//原理：名字改编
//特点：当函数名字相同的时候，会按照函数的名字，
//加上参数列表进行改名，将函数的名字与参数列表
//合在一起重新取名(函数的名字加上参数的个数
//参数的顺序、参数的类型)
int add(int x, int y)
{
    return x + y;
}

float add(float x, float y)
{
    return x + y;
}

int add(int x, float y)
{
    return x + y;
}

int add(float x, int y)
{
    return x + y;
}

int add(int x, int y, int z)
{
    return x + y + z;
}

float add(float x, float y, float z)
{
    return x + y + z;
}

void test()
{
    int * pInt= static_cast<int *>(malloc(sizeof(int)));
    memset(pInt, 0, sizeof(int));

    free(pInt);
    pInt = nullptr;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

