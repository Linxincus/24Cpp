#include <iostream>

using std::cout;
using std::endl;

//标准命名空间也是可以进行扩展的
namespace std
{
struct MyStruct
{
int a;
double b;
};

}//end of namespace std

//重要：命名空间是可以进行扩展的
namespace  wd
{
int number = 100;
//带命名空间的函数声明
void print();
}

namespace  wh
{

void display()
{
    cout << "void wh::display()" << endl;
    //想在display中调用wd中的print
    wd::print();
}

void func()
{
    cout << "void wh::func()" << endl;
}
}//end of namespace wh

namespace wd
{

int number2 = 200;
void print()
{
    cout << "void wd::print()" << endl;
    //想在print中调用wh中的func
    wh::func();
}

}//end of namespace wd

int main(int argc, char *argv[])
{
    return 0;
}

