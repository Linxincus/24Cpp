#include <iostream>

using namespace std;

int number = 100;

void print()
{
    cout << "void print()" << endl;
}

//写了命名空间wd之后
namespace  wd
{
//命名空间中可以定义变量、函数、结构体、类等，将其称为实体
int number = 300;

void print()
{
    cout << "void wd::print()" << endl;
}

}//end of namespace wd

int main(int argc, char *argv[])
{
    cout << "number = " << number << endl;
    return 0;
}

