#include <iostream>

using std::cout;
using std::endl;

//宏定义发生的时机在预处理阶段，会进行字符串的替换
//如果发生了错误，只有到了运行时才会发现
#define MAX 10
#define MULTIPLY(x, y) ((x) * (y))

void test()
{
    int a = 3;//初始化
    a = 4;//赋值

    //const定义的常量发生的时机在编译的时候,如果有错误就可以
    //在编译的时候发现
    //常量在定义的时候，必须要进行初始化
    /* const int number = 10;//C++中定义常量的语法 */
    //在C++中将char/short/int/float/double/long称为内置类型
    //自定义类型
    int const number = 10;//C++中定义常量的语法
    /* number = 20;//error, 赋值,常量是不能赋值的 */

    cout << "MULTIPLY(1 + 2, 3 + 4) = " 
         << MULTIPLY(1 + 2, 3 + 4) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

