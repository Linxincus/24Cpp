#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;
using std::remove_if;

void func(int &value)
{
    cout << value << "  ";
}

/* bool func3(int value) */
/* { */
/*     return value > 5; */
/* } */

void test()
{
    vector<int> vec = {1, 3, 5, 7, 8, 9, 4, 3, 2};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl;
    int a = 10;
    //lambda表达式--->匿名函数
    //[],叫做捕获列表
    auto it = remove_if(vec.begin(), vec.end(), 
                        /* [a](int value)mutable->bool{ */
                        /* [&a](int value)->bool{ */
                        [&a](int value){
                        ++a;
                        cout << "a = " << a << endl;
                        return value > 5;
                        });
    vec.erase(it, vec.end());

    for_each(vec.begin(), vec.end(), func);
    cout << endl;
    cout << "a = " << a << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

