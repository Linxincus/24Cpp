#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class String
{
public:
    String()
    : _pstr(new char[5]() + 4)
    {
        cout << "String()" << endl;
        initRefCount();
    }

    String(const char *pstr)
    : _pstr(new char[strlen(pstr) + 5]() + 4)
    {
        cout << "String(const char *)" << endl;
        strcpy(_pstr, pstr);
        initRefCount();
    }

    //String s2 = s1;
    String(const String &rhs)
    : _pstr(rhs._pstr)//浅拷贝
    {
        cout << "String(const String &)" << endl;
        increaseRefCount();
    }

    //s3 = s2
    String &operator=(const String &rhs)
    {
        cout << "String &operator=(const String &)" << endl;
        if(this != &rhs)//1、自复制
        {
            //shift + *  n
            release();//释放左操作数
            
            _pstr = rhs._pstr;//3、浅拷贝
            increaseRefCount();
        }
        return *this;//4、返回*this
    }

private:
    class CharProxy
    {
    public:
        CharProxy(String &self, size_t idx)
        : _self(self)
        , _idx(idx)
        {

        }
    public:
        char &operator=(const char &ch);
        friend std::ostream &operator<<(std::ostream &os, const CharProxy &rhs);
    private:
        String &_self;
        size_t _idx;
    };

public:
    //CharProxy就替代了以前的返回类型是char的问题
    CharProxy operator[](size_t idx)//String * const this ,idx
    {
        return CharProxy(*this, idx);
    }
#if 0
    //s3[0] = 'H'
    char &operator[](size_t idx)
    {
        if(idx < size())
        {
            //是不是共享
            if(getRefCount() > 1)
            {
                char *ptmp = new char[size() + 5]() + 4;
                strcpy(ptmp, _pstr);
                decreaseRefCount();

                _pstr = ptmp;
                initRefCount();
            }
            return _pstr[idx];
        }
        else
        {
            static char nullchar = '\0';
            return nullchar;
        }
    }
#endif

    ~String()
    {
        cout << "~String()" << endl;
        release();
    }

private:
    void release()
    {
        decreaseRefCount();
        if(0 == getRefCount())//2、释放左操作数
        {
            delete [] (_pstr - 4);
        }
    }

    void initRefCount()
    {
        *(int *)(_pstr - 4) = 1;
    }

    void increaseRefCount()
    {
        ++*(int *)(_pstr - 4);
    }

    void decreaseRefCount()
    {
        --*(int *)(_pstr - 4);
    }

    size_t size() const
    {
        return strlen(_pstr);
    }

public:
    const char *c_str() const
    {
        return _pstr;
    }

    int getRefCount() const
    {
        return *(int *)(_pstr - 4); 
    }

    friend std::ostream &operator<<(std::ostream &os, const String &rhs);
    friend std::ostream &operator<<(std::ostream &os, const CharProxy &rhs);
private:
    char *_pstr;
};

std::ostream &operator<<(std::ostream &os, const String &rhs)
{
    if(rhs._pstr)
    {
        os << rhs._pstr;
    }

    return os;
}

//写操作
char &String::CharProxy::operator=(const char &ch)
{
    if(_idx < _self.size())
    {
        //是不是共享
        if(_self.getRefCount() > 1)
        {
            char *ptmp = new char[_self.size() + 5]() + 4;
            strcpy(ptmp, _self._pstr);
            _self.decreaseRefCount();

            _self._pstr = ptmp;
            _self.initRefCount();
        }
        _self._pstr[_idx] = ch;//赋值操作
        return _self._pstr[_idx];
    }
    else
    {
        static char nullchar = '\0';
        return nullchar;
    }

}

//需要作为CharProxy与String的双友元
std::ostream &operator<<(std::ostream &os, const String::CharProxy &rhs)
{
    os << rhs._self._pstr[rhs._idx];

    return os;
}
void test()
{
    //如果要直接打印引用计数?
    String s1("hello");
    String s2 = s1;
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    printf("&s1 = %p\n", s1.c_str());
    printf("&s2 = %p\n", s2.c_str());
    cout << "s1.getRefCount() = " << s1.getRefCount() <<endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() <<endl;

    cout << endl;
    String s3("world");
    cout << "s3 = " << s3 << endl;
    printf("&s3 = %p\n", s3.c_str());
    cout << "s3.getRefCount() = " << s3.getRefCount() <<endl;

    cout << endl;
    s3 = s1;
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    printf("&s1 = %p\n", s1.c_str());
    printf("&s2 = %p\n", s2.c_str());
    printf("&s3 = %p\n", s3.c_str());
    cout << "s1.getRefCount() = " << s1.getRefCount() <<endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() <<endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() <<endl;

    cout << endl << "对s3[0]执行写操作" <<endl;
    /* s3[0] = 'H';//char = char,CharProxy = char */
    s3.operator[](0).operator=('H');

    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    printf("&s1 = %p\n", s1.c_str());
    printf("&s2 = %p\n", s2.c_str());
    printf("&s3 = %p\n", s3.c_str());
    cout << "s1.getRefCount() = " << s1.getRefCount() <<endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() <<endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() <<endl;

    //代码的问题在哪里？
    //不能区分读与写操作
    cout << endl << "对s1[0]执行读操作" << endl;
    cout << "s1[0] = " << s1[0] << endl;//cout << CharProxy
    /* operator<<(cout, "s1[0] = "); */
    /* operator<<(cout, s1.operator[](0)); */
    /* operator<<(operator<<(cout, "s1[0] = "), s1.operator[](0)).operator<<(endl); */
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    printf("&s1 = %p\n", s1.c_str());
    printf("&s2 = %p\n", s2.c_str());
    printf("&s3 = %p\n", s3.c_str());
    cout << "s1.getRefCount() = " << s1.getRefCount() <<endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() <<endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() <<endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

