#include <iostream>

using std::cout;
using std::endl;

template <typename T>
class Test
{
public:
    template <typename K>
    virtual K add(K x, K y)
    {
        return x + y;
    }
private:
    T _x;
};

void test()
{

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

