#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::unique_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void print() const
    {
        cout << "(" << _ix 
             << ", " << _iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0()
{
    int *pInt = new int(10);
    unique_ptr<int> up(pInt);
    unique_ptr<int> up2(pInt);
}

void test()
{
    //问题的本质：使用了不同的智能指针托管同一块堆空间
    Point *pt = new Point(1, 2);
    unique_ptr<Point> up(pt);
    unique_ptr<Point> up2(pt);
}

void test2()
{
    //问题的本质：使用了不同的智能指针托管同一块堆空间
    unique_ptr<Point> up(new Point(1, 2));
    cout << "up = ";
    up->print();

    cout << endl;
    unique_ptr<Point> up2(new Point(3, 4));
    cout << "up2 = ";
    up2->print();

    cout << endl;
    up.reset(up2.get());
    cout << "up = ";
    up->print();
    cout << "up2 = ";
    up2->print();
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

