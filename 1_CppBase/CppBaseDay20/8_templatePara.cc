#include <iostream>

using std::cout;
using std::endl;

//模板的参数
//1、T就是类型参数
//2、非类型参数,必须是整型（char/bool/short/int/long/size_t）
//注意：不能使用浮点型(float/double)
//注意：函数的参数是可以设定默认值的，同时模板的参数也是可以
//设定默认值的
template <typename T = int, short kSize = 10>
T func(T x, T y)
{
    return x * y * kSize;
}

void test()
{
    int ia = 10, ib = 20;
    cout << "func(ia, ib) = " << func<int, 10>(ia, ib) << endl;
    cout << "func(ia, ib) = " << func<int, 40>(ia, ib) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

