#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//编码规范
//
//在C++中将struct的功能做了提升，结构体中既可以定义数据成员
//也可以定义成员函数，struct与class的区别是：默认访问权限
//struct的默认访问权限是共有的public，而class的默认访问权限
//是私有的private

class Comupter//类名 
{
//类内部：就是类名后的左大括号，到最后分号前面右大括号
public:    
    //成员函数
    //设置电脑的品牌
    inline
    void setBrand(const char *brand)
    {
        //n + shift + k
        strcpy(_brand, brand);//没有考虑内存越界
    }
    
    //设置电脑的价格
    inline
    void setPrice(float price)
    {
        _price = price;
    }
    
    //打印电脑的品牌与价格
    inline
    void print()
    {
        cout << "brand = " << _brand << endl
             << "price = " << _price << endl;
    }

private://私有的，可以体现封装性，只能在类内进行使用
    //数据成员
    char _brand[20];//品牌brand_ m_nBrand[]
    /* char *_brand;//品牌 */
    float _price;//价格price_   m_nPrice;
};//一定不要忘了，表明这一句结束了

int main(int argc, char *argv[])
{
    //通过类名Computer创建了对象com
    Comupter com;
    com.setBrand("Thinkpad");
    com.setPrice(5500);
    com.print();
    /* com._price = 5500;//error */

    return 0;
}

