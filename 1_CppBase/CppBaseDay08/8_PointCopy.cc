#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    //默认情况下，编译器会生成拷贝构造函数
    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << ", " << _iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    int a = 10;
    int b = a;

    Point pt1(1, 2);
    cout << "pt1 = ";
    pt1.print();

    cout << endl;
    //pt2也是一个新对象，也会调用构造函数
    Point pt2 = pt1;//拷贝构造函数
    cout << "pt2 = ";
    pt2.print();
}

void func(Point pt)//Point pt = pt2
{
    cout << "pt = ";
    pt.print();
}

void test2()
{
    Point pt2(3, 4);
    cout << "pt2 = ";
    pt2.print();

    cout << endl;
    func(pt2);//拷贝构造函数
}

//int func()
Point func2()
{
    Point pt2(4, 5);
    cout << "pt2 = ";
    pt2.print();

    return pt2;//满足拷贝构造函数的调用时机三
}

void test3()
{
    //func2()返回值是一个临时对象
    //临时对象的生命周期只在本行
    Point pt = func2();
    cout << "pt = ";
    pt.print();
}

int main(int argc, char *argv[])
{
    test3();
    return 0;
}

