#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    //无参构造函数（默认构造函数）编译器会自动提供
    //如果自己实现了构造函数，那么编译器就不会再给我们
    //提供默认的无参构造函数；如果还想使用编译器提供的默认
    //构造函数，就需要自己显示的写出来
    //
    //构造函数是可以进行重载的
    Point()
    : _ix(0)
    , _iy(0)
    {
        cout << "Point()" << endl;
    }
    
    //构造函数
    //构造函数的作用：就是为了初始化对象的数据成员的
    //构造函数不用也不能写返回类型的
    Point(int ix, int iy)//:_ix(ix), _iy(iy)
    : _ix(ix)//冒号：初始化表达式或者初始化列表
    ,_iy(iy)
    {
        /* int a = 10; */
        /* int b = a; */
        /* int c(a);//int c = a; */
        /* int d = a;//初始化 */
        /* d = a;//赋值 */
        /* _ix = ix;//赋值 */
        cout << "Point(int, int)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << ", " << _iy
             << ")" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    //创建对象的时候，会调用构造函数
    //进行数据成员的初始化
    //当用类创建对象的时候，构造函数会被自动调用
    Point pt(1, 2);
    Point pt2;
    pt.print();

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

