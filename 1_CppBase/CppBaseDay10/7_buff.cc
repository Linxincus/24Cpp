#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;
using std::flush;
using std::ends;

void test()
{
    //在ubuntu1804上，缓冲区大小是1024
    for(size_t idx = 0; idx < 1024; ++idx)
    {
        cout << 'a';
    }
    //endl、flush、ends到底是什么？函数
    /* operator<<(cout, 'b').operator<<(endl); */
    /* operator<<(cout, 'b'); */
    /* cout.operator<<(endl); */
    cout << 'b' << endl;//刷新并且换行

    /* operator<<(cout, 'b').operator<<(flush); */
    cout << "hello" << flush;//只刷新，不换行
    cout << "wangdao" << ends;//没有刷新，也没有换行
    sleep(3);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

