#include <string.h>
#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

//需求：只能生成栈对象，不能生成堆对象
//解决方案：将operator new/delete设置为私有的

class Student
{
public:
    Student(const char *name, int id)
    : _name(new char[strlen(name) + 1]())
    , _id(id)
    {
        cout << "Student(const char *, int)" <<endl;
        strcpy(_name, name);
    }
private:    
    //申请原始的未初始化的空间
    static void *operator new(size_t sz)
    {
        cout << "void *operator new(size_t )" << endl;
        //函数体该怎么实现呢
        void *pret = malloc(sz);

        return pret;
    }

    static void operator delete(void *ptr)
    {
        cout << "void operator delete(void *)" << endl;
        free(ptr);
    }
public:

    void print() const
    {
        if(_name)
        {
            cout << "name: " << _name << endl; 
        }
        cout << "id: " << _id << endl;
    }

/* private: */
    //dt)  = del to )
    ~Student()
    {
        cout << "~Student()" << endl;
        if(_name)
        {
            delete [] _name;
            _name = nullptr;
        }
    }

private:
    char *_name;
    int _id;
};

void test()
{
    //Q:创建栈对象需要哪些条件呢?
    //A:构造函数与析构函数都需要是public
    Student stu("xiaohong", 4201);//栈对象,ok
    stu.print();

    /* Student *pstu = new Student("lili", 4321);//堆对象,error */
    /* pstu->print(); */

    //Q：对象的销毁与析构函数的执行是不是等价的?
    //A:不等价，对于堆对象而言，析构函数的执行只是对象销毁
    //中的其中一个步骤,除此之外，对象销毁还会将对象本身占用
    //的内存回收掉
    //对于栈对象而言，对象的销毁与析构函数的执行是等价的
    /* delete pstu; */
    /* pstu = nullptr; */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

