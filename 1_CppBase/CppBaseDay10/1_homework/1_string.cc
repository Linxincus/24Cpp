#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//实现本代码的方式
//1、写一个测试一个(建议这种方式)
//2、将所有的代码写完之后再进行测试

//作业中关于编程题的结果
//建议：将代码、注释、测试用例、运行截图都传到作业系统中
class String
{
public:
	String()
    : _pstr(nullptr)//将指针初始化为空
    /* : _pstr(new char[1]())//将指针初始化为空 */
    {
        cout << "String()" << endl;
    }

	String(const char *pstr)
    : _pstr(new char[strlen(pstr) + 1]())
    {
        cout << "String(const char *)" << endl;
        strcpy(_pstr, pstr);
    }

	String(const String & rhs)
    : _pstr(new char[strlen(rhs._pstr) + 1]())
    {
        cout << "String(const String &)" << endl;
        strcpy(_pstr, rhs._pstr);
    }

	String & operator=(const String & rhs)
    {
        cout << "String &operator=(const String &)" << endl;
        if(this != &rhs)//1、自复制
        {
            //2、释放左操作数
            delete [] _pstr;
            _pstr = nullptr;

            //3、深拷贝
            _pstr = new char[strlen(rhs._pstr) + 1]();
            strcpy(_pstr, rhs._pstr);
        }
        //4、返回*this
        return *this;
    }

	~String()
    {
        cout << "~String()" << endl;
        if(_pstr)
        {
            delete [] _pstr;
            _pstr = nullptr;
        }
    }

	void print() const
    {
        if(_pstr)
        {
            cout << "_pstr = " << _pstr << endl;
        }
    }

    const char *c_str()
    {
        return _pstr;
    }

private:
	char *_pstr;
};

int main(void)
{
	String str1;//无参构造函数
	str1.print();
	
    cout << endl;
    //    C++         C
	String str2 = "Hello,world";//String("Hello,world"),隐式转换
	String str3("wangdao");//传递是C风格字符串
	
	str2.print();		
	str3.print();	
	
    cout << endl;
	String str4 = str3;//拷贝构造函数
	str4.print();
	
    cout << endl;
	str4 = str2;//赋值运算符
	str4.print();
	
	return 0;
}
