#include <iostream>

using std::cout;
using std::endl;

int func1(int x, int y)
{
    cout << "int func1(int, int)" << endl;
    return x + y;
}

int func2(int a, int b)
{
    cout << "int func2(int, int)" << endl;
    return a * b;
}

void test()
{
    int (*pf)(int, int);//函数指针

    pf = &func1;
    cout << "pf(100, 200) = " << pf(100, 200) << endl;

    cout << endl;
    pf = func2;
    cout << "pf(10, 50) = " << pf(10, 50) << endl;
}

void func4(int (*pt)(int, int))
{
    cout << "func4 = " << pt(2000, 8000) << endl;
}

int main(int argc, char *argv[])
{
    func4(func1);
    /* test(); */
    /* printf("main = %p\n", main); */
    /* printf("&main = %p\n", &main); */
    return 0;
}

