#include <stdio.h>

//声明了函数add
int add(int, int);

int main(int argc, char *argv[])
{
    int a = 10;
    int b = 20;

    printf("add(10, 20) = %d\n", add(a, b));
    return 0;
}

