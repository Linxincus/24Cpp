#include <iostream>

using std::cout;
using std::endl;

//i^2 = -1

class Complex
{
public:
    Complex(double dreal = 0.0, double dimag = 0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout << "Complex(double = 0.0, double = 0.0)" << endl;
    }

    void print() const
    {
        cout << _dreal << " + " << _dimag << "i" << endl;
    }

    ~Complex()
    {
        cout << "~Complex()" << endl;
    }
private:
    double _dreal;
    double _dimag;
};

void test()
{
    Complex c1(1, 3);
    cout << "c1 = ";
    c1.print();

    cout << endl;
    Complex c2(2, 6);
    cout << "c2 = ";
    c2.print();

    //int a = 3;
    //int b = 4;
    //int c = a + b;
    cout << endl;
    Complex c3 = c1 + c2;
    cout << "c3 = ";
    c3.print();
}

int operator+(int x, int y)
{

}

Z = X + Y;
x * y;
x/y;
void test2()
{
    //如果a = 1
    if(a || b)
    {
        cout << "11111" << endl;
    }
    else
    {
        cout << "2222" << endl;
    }
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

