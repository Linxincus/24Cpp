#include <iostream>

using std::cout;
using std::endl;

class Complex
{
public:
    Complex(double dreal = 0.0, double dimag = 0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout << "Complex(double = 0.0, double = 0.0)" << endl;
    }

    double getReal() const
    {
        return _dreal;
    }

    double getImag() const
    {
        return _dimag;
    }

    void print() const
    {
        cout << _dreal << " + " << _dimag << "i" << endl;
    }

    ~Complex()
    {
        cout << "~Complex()" << endl;
    }
private:
    double _dreal;
    double _dimag;
};

//运算符重载的第一种形式：普通函数
Complex operator+(const Complex &lhs, const Complex &rhs)
{
    cout << "Complex operator+(const Complex &, const Complex &)" << endl;
    Complex com(lhs.getReal() + rhs.getReal(),
                lhs.getImag() + rhs.getImag());

    return com;
}

void test()
{
    Complex c1(1, 3);
    cout << "c1 = ";
    c1.print();

    cout << endl;
    Complex c2(2, 6);
    cout << "c2 = ";
    c2.print();

    //int a = 3;
    //int b = 4;
    //int c = a + b;
    cout << endl;
    Complex c3 = c1 + c2;
    cout << "c3 = ";
    c3.print();
}


int main(int argc, char *argv[])
{
    test();
    return 0;
}

