#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using std::cout;
using std::endl;
using std::vector;
using std::for_each;
using std::mem_fn;
using std::remove_if;
using std::bind;

class Number
{
public:
    Number(size_t data = 0)
    : _data(data)
    {
    }

    void print() const
    {
        cout << _data << "  ";
    }

    bool isEven() const
    {
        return (0 == _data % 2);
    }

    bool isPrimer() const
    {
        if(1 == _data)
        {
            return false;
        }
        for(size_t idx = 2; idx <= _data/2; ++idx)
        {
            if(0 == _data % idx )
            {
                return false;
            }
        }

        return true;
    }

    ~Number()
    {

    }
private:
    size_t _data;
};
void test()
{
    vector<Number> vec;

    for(size_t idx = 1; idx != 30; ++idx)
    {
        vec.push_back(Number(idx));
    }
    //遍历vector中的元素
    /* for_each(vec.begin(), vec.end(), mem_fn(&Number::print)); */
    Number num(3);
    using namespace std::placeholders;
    for_each(vec.begin(), vec.end(), bind(&Number::print, _1));
    cout << endl;

    //将所有的偶数都删除
    cout << endl;
    vec.erase(remove_if(vec.begin(), vec.end(), 
                        mem_fn(&Number::isEven)), vec.end());
    for_each(vec.begin(), vec.end(), mem_fn(&Number::print));
    cout << endl;
    
    //将所有的质数删除
    cout << endl;
    vec.erase(remove_if(vec.begin(), vec.end(), 
                        mem_fn(&Number::isPrimer)), vec.end());
    for_each(vec.begin(), vec.end(), mem_fn(&Number::print));
    cout << endl;

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

