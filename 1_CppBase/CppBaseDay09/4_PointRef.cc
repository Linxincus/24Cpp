#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    , _iz(_ix)
    {
        cout << "Point(int = 0, int = 0)" << endl;
        /* _ix = ix;//赋值 */
        /* _iy = iy;//赋值 */
    }

    void setZ(int iz)
    {
        //改变了引用的值，那么绑定的变量_ix的值也会发生改变
        _iz = iz;
    }

    void print()
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ", " << this->_iz
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
    int &_iz;//实质就是一个指针，指针常量,引用会占用一个指针
             //大小的空间
};

void test()
{
    cout << "sizeof(int) = " << sizeof(int) << endl;
    cout << "sizeof(Point) = " << sizeof(Point) << endl;

    cout << endl;
    Point pt(1, 2);
    cout << "pt = ";
    pt.print();

    cout << endl;
    pt.setZ(100);
    cout << "pt = ";
    pt.print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

