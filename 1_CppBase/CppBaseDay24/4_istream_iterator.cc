#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::cin;
using std::istream_iterator;
using std::ostream_iterator;
using std::vector;
using std::copy;

void test()
{
    vector<int> vec;
    /* vec.reserve(10); */
    /* vec[0] = 10;//error */
    istream_iterator<int> isi(cin);
    //对于vector而言，插入元素一般使用的是push_back
    /* copy(isi, istream_iterator<int>(), vec.begin()); */
    copy(isi, istream_iterator<int>(), 
         std::back_inserter(vec));

    copy(vec.begin(), vec.end(), 
         ostream_iterator<int>(cout, "\n"));
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

