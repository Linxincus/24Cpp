#include <iostream>

using std::cout;
using std::endl;

//作用域与可见域
//如果没有发生屏蔽现象，可见域与作用域是相等的；
//如果发生屏蔽现象，那么作用域是大于可见域
int number = 1;//全局变量

namespace wd
{
int number = 20;//命名空间中的实体

class Example
{
public:
    Example(int value)
    : number(value)
    {
    }

    void print(int number)//函数的形参
    {
        cout << "形参number = " << number << endl;
        cout << "数据成员number = " << this->number << endl;
        cout << "数据成员number = " << Example::number << endl;
        cout << "命名空间中的number = " << wd::number << endl;
        cout << "全局变量number = " << ::number << endl;//匿名命名空间
    }
private:
    int number;//数据成员
};//end of class Example

}//end of namespace wd

int main(int argc, char *argv[])
{
    int ix = 4000;
    wd::Example ex(300);
    ex.print(ix);
    return 0;
}

