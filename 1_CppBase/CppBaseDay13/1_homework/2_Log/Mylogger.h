#ifndef __MYLOGGER_H__
#define __MYLOGGER_H__

#include <iostream>
#include <log4cpp/Category.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

//单例模式的特点：一个类只能创建一个对象
class Mylogger
{
public:
    static Mylogger *getInstance();
    static void destroy();

	void warn(const char *msg);
	void error(const char *msg);
	void debug(const char *msg);
	void info(const char *msg);

private:
	Mylogger();
	~Mylogger();

private:
    static Mylogger *_pInstance;
    Category &_root;//日志记录器是唯一的
};
//-------
#endif
