#include <iostream>

using std::cout;
using std::endl;

class Complex
{
    friend Complex operator+(const Complex &lhs, const Complex &rhs);
public:
    Complex(double dreal = 0.0, double dimag = 0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout << "Complex(double = 0.0, double = 0.0)" << endl;
    }

    Complex &operator+=(const Complex &rhs)
    {
        cout << "Complex &operator+=(const Complex &)" << endl;
        _dreal += rhs._dreal;
        _dimag += rhs._dimag;

        return *this;
    }

    //前置++
    Complex &operator++()
    {
        cout << "Complex &operator++()" << endl;
        ++_dreal;
        ++_dimag;

        return *this;
    }

    //后置++
    Complex operator++(int)//int只起到标识作用,没有传参含义
    {
        cout << "Complex operator++(int)" << endl;
        Complex com(*this);//存++之前的值
        _dreal++;
        _dimag++;

        return com;
    }


    double getReal() const
    {
        return _dreal;
    }

    double getImag() const
    {
        return _dimag;
    }

    void print() const
    {
        cout << _dreal << " + " << _dimag << "i" << endl;
    }

    ~Complex()
    {
        cout << "~Complex()" << endl;
    }
private:
    double _dreal;
    double _dimag;
};

Complex operator+(const Complex &lhs, const Complex &rhs)
{
    cout << "friend Complex operator+(const Complex &, const Complex &)" << endl;
    return Complex(lhs._dreal + rhs._dreal, lhs._dimag + rhs._dimag);
}

void test()
{
    Complex c1(1, 3);
    cout << "c1 = ";
    c1.print();

    cout << endl;
    Complex c2(2, 6);
    cout << "c2 = ";
    c2.print();

    cout << endl;
    Complex c3 = c1 + c2;//4 + 9i
    cout << "c3 = ";
    c3.print();

    int c = 10;
    //表达式的值与变量c的值
    cout << "++c表达的值 = " << ++c 
        << ",变量c的值 = " << c << endl;//11 11
    cout << "c++表达的值 = " << c++
        << ",变量c的值 = " << c << endl;//11 12
    cout << endl;

    cout << endl << endl;
    cout << "++c3 = ";
    (++c3).print();//4 + 10i
    cout << "c3 = ";
    c3.print();//4 + 10i

    /* c3++ */
    cout << endl << endl;
    cout << "c3++ = ";
    (c3++).print();//4 + 10i
    cout << "c3 = ";
    c3.print();//5 + 11i

    /* &++c3;//ok */
    /* &(c3++);//error */

}


int main(int argc, char *argv[])
{
    test();
    return 0;
}

