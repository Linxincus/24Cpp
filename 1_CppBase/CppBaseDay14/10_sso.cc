#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

void test()
{
    int *pInt = new int(10);
    string s1("hello");
    string s2("helloworldwuhan");
    string s3("welcometowuhanwangdao");
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    printf("&s1 = %p\n", s1.c_str());
    printf("&s2 = %p\n", s2.c_str());
    printf("&s3 = %p\n", s3.c_str());
    printf("&pInt = %p\n", &pInt);
    printf("pInt = %p\n", pInt);

    delete pInt;
    pInt = nullptr;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

