#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

void func()
{
    cout <<"void func()" << endl;
}

void test()
{
    //对于注册是函数func，在进程正常结束之后，会被调用
    atexit(func);
    atexit(func);
    atexit(func);
    atexit(func);
}

int main(int argc, char *argv[])
{
    cout << "begin main..." << endl;
    test();
    cout << "finish main..." << endl;
    return 0;
}




